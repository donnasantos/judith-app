let registerUsername = "";
let registerPassword = "";

const registerBtn = document.getElementById('registerBtn');

registerBtn.addEventListener('click', function(){
	const registerUsernameValue = document.getElementById('registrationUsername').value;
	const registerPasswordValue = document.getElementById('registrationPassword').value;

	if(registerUsernameValue === "" || registerPasswordValue === ""){
		alert("Please input Username and Password.");
		return;
	}

	if(registerPasswordValue.length < 8){
		alert("Password length must be 8 or longer");
		return;
	}

	registerUsername = registerUsernameValue;
	registerPassword = registerPasswordValue;

	const registerScreen = document.getElementById('inputRegistration');
	registerScreen.remove();

	const loginScreen = document.getElementById('inputLogin');
	loginScreen.style.display = loginScreen.style.display === 'none' ? '' : 'none';
	
});



	const loginBtn = document.getElementById('loginBtn')

	loginBtn.addEventListener('click', function(){

    const loginUsernameValue = document.getElementById('loginUsername').value;
    const loginPasswordValue = document.getElementById('loginPassword').value;


    loginUsername = loginUsernameValue;
	loginPassword = loginPasswordValue;

    if (loginUsernameValue === "" || loginPasswordValue === "") {
        alert ("Please input Username and Password.");
        return;
    }

    else if (loginUsername !== registerUsername || loginPassword !== registerPassword) {
        alert("Incorrect Username and Password.");
        return;

    }
    else if (loginUsername === registerUsername && loginPassword === registerPassword) {

    	window.location.href = "homepage/homepage.html";  
	
    }
});